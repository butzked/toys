#Modul 'pylolaos' (import as lola)
#Elaborated by Daniel Butzke, 2019 - 
#Contact: dtzke@web.de
#
#
def go():
    print("\n\033[92m" + "All components are prepared to supply. Here we go!" + "\033[0m")
    #
    input("\nPlease press 'ENTER' to continue ...\n")
#
#
def MH2MeSH(dictionary):
    """Structurally corrects the MH retrieved from NCBI-Entrez to MeSH usually retrieved from PubMed-GUI.
    Also removes MAJR-indices (*).
    Works upon a dictionary: needs input-value (list of strings) from key["MH"] and
    adds new key["MeSH"] with output-value (list of corrected strings)."""
    
    if "MH" in dictionary:
        dictionary["MeSH"] = []
        for string in dictionary["MH"]:
            term = string.replace("*", "")
            sep0 = term.find("/")
            if sep0 == -1: #if no occurence of substring is found, string.find() returns -1
                dictionary["MeSH"] += [term]
                continue
            else:
                sep1 = term.find("/", sep0+1)
                term = term + " "
                mesh1 = term[:sep0] + term[sep0:sep1]
                dictionary["MeSH"] += [mesh1]
            if sep1 == -1:
                continue
            else:
                sep2 = term.find("/", sep1+1)
                mesh2 = term[:sep0] + term[sep1:sep2]
                dictionary["MeSH"] += [mesh2]
            if sep2 == -1:
                continue
            else:
                sep3 = term.find("/", sep2+1)
                mesh3 = term[:sep0] + term[sep2:sep3]
                dictionary["MeSH"] += [mesh3]
            if sep3 == -1:
                continue
            else:
                sep4 = term.find("/", sep3+1)
                mesh4 = term[:sep0] + term[sep3:sep4]
                dictionary["MeSH"] += [mesh4]
            if sep4 == -1:
                continue
            else:
                sep5 = term.find("/", sep4+1)
                mesh5 = term[:sep0] + term[sep4:sep5]
                dictionary["MeSH"] += [mesh5]
            if sep5 == -1:
                continue
    else:
        dictionary["MH"] = []
        dictionary ["MeSH"] = []
#
#
def meta4pmid():
    """Retrieval of metadata for an inputted PMID from PubMed.
    Returns pmid and corrected metadata in a dictionary (ref_dict).
    If no valid PMID is entered ref_dict contains an error message as the only value (key = ["id:"]).
    Entry of a valid PMID is repeated then (2 times).
    Furthermore, ref_dict["MH"] is structurally corrected to MeSH (new key = ["MeSH"])
    by running function 'MH2MeSH(ref_dict)' and
    "stickyness" (number of similar articles in PubMed) is added (key = ["STCK"]).
    Relies on Biopython (Entrez, Medline) and function 'MH2MeSH(dictionary)'."""
    
    from Bio import Entrez, Medline
    #
    Entrez.email = "..."
    #
    ref_dict = {"id:": ""}
    q = 0
    while "id:" in ref_dict:
        q += 1
        pmid = input("\n\tPlease ENTER a valid PMID: ")
        record = list(Medline.parse(Entrez.efetch(db="pubmed", id=pmid, rettype="medline", retmode="text")))
        ref_dict = record[0]
        del record[1:]
        if q == 4:
            print("\33[91m" + "\n\nPlease REFER TO PubMed for a valid PMID!" + "\33[0m\n")
            raise KeyboardInterrupt
    #    
    b_record = Entrez.read(Entrez.elink(db="pubmed", id=pmid, cmd="neighbor_score"))
    if b_record[0]["LinkSetDb"][0]["Link"]:
        ref_dict["STCK"] = len(b_record[0]["LinkSetDb"][0]["Link"])
        del b_record
    else:
        ref_dict["STCK"] = 0
    #
    if "TI" not in ref_dict:
        ref_dict["TI"] = ""
    if "AB" not in ref_dict:
        ref_dict["AB"] = ""
    #
    MH2MeSH(ref_dict)
    #    
    return pmid, ref_dict
#
#
def print_biblio(dictionary):
    print("\n\033[94m" + "TITLE: " + "\033[0m", end=" ")
    print(dictionary["TI"])
    print("\n\033[94m" + "AUTHORs: " + "\033[0m", end=" ")
    print(", ".join(dictionary["AU"]))
    print("\n\033[94m" + "SOURCE: " + "\033[0m", end=" ")
    print(dictionary["SO"])
    print("\n")
#
#
def confirm_ref():
    answer = input("\n\tPlease CONFIRM reference document! Y = Yes!, N = No!: ")
    if answer.lower() == "n":
        print("\n\n\t\033[31m" + "Please RESTART GSPsearch with 'Run All Cells'!" + "\033[0m\n")
        raise KeyboardInterrupt
    else:
        print("\n")
#
#
def get_pos():
    """Returns user input as a string of comma-separated PMIDs."""
    positives = input("\n\tYou Can ENTER positives (PMIDs separated by \", \"): ")
    print("\n")
    return positives
#
#
def sims4pmid(pmid):
    """Retrieval of Similar Articles for a given valid PMID from PubMed (pmid).
    Validity of PMID is not checked!
    Returns number of Similar Articles (sims_count) and PMIDs, Similarity Scores
    in a list of dictionaries (sims_dict_list, dict_keys: ["Id"], ["Score"]).
    CAUTION: Here, the reference document (pmid) is not included in the list!
    Relies on Biopython (Entrez)."""
    
    from Bio import Entrez
    #
    record = Entrez.read(Entrez.elink(db="pubmed", id=pmid, cmd="neighbor_score"))
    if record[0]["LinkSetDb"][0]["Link"]:
        sims_dict_list = record[0]["LinkSetDb"][0]["Link"]
        del record[0]["ERROR"], record[0]["LinkSetDbHistory"], record[0]["DbFrom"], record[0]["IdList"]
    else:
        sims_dict_list = []
    #
    for elem in sims_dict_list:
        elem['Score'] = int(elem['Score'])
    #
    sims_count = len(sims_dict_list)
    #
    return sims_count, sims_dict_list
#
#
def outlier_ref(sims_count, dictionary):
    """Function to check some features of the metadata of a given citation.
    Checks are passed, if the citation metadata is within certain boundaries."""
    if sims_count > 500:
        print("\033[91m" + "\nReference is too \"sticky\" to be used with GSPsearch." + "\033[0m")
        print("\033[91m" + "Please proceed in PubMed." + "\033[0m")
        raise KeyboardInterrupt
    else:
        print("\033[92m" + "\nReference check 1: passed ..." + "\033[0m")
#
    if not dictionary["AB"]:
        print("\033[91m" + "\nReference lacks an abstract!" + "\033[0m")
        print("\033[91m" + "Please proceed in PubMed." + "\033[0m")
        raise KeyboardInterrupt
    else:
        print("\033[92m" + "\nReference check 2: passed ..." + "\033[0m")
#
    import nltk_data
    from nltk_data_tokenize__init__ import word_tokenize
    if dictionary["AB"] and dictionary["TI"]:
        ref_ab_ti2 = (dictionary["AB"] + dictionary["TI"] + dictionary["TI"]) #Titel wird 2 mal berücksichtigt! (siehe PubMed_Help: ...)
        ref_length = len(word_tokenize(ref_ab_ti2.lower()))
    if ref_length > 500:
        print("\033[91m" + "\nAbstract of reference is too long to be used with GSPsearch." + "\033[0m")
        print("\033[91m" + "Please proceed in PubMed." + "\033[0m")
        raise KeyboardInterrupt
    else:
        print("\033[92m" + "\nReference check 3: passed ..." + "\033[0m")
#
    del ref_ab_ti2
    #
    return ref_length
#
#
def meta4sims(sims_pre_dict_list):
    """Retrieval of more metadata  for the Similar Articles downloaded from PubMed with the function 'sims4pmid()'
    and entry into the respective dictionaries stored in a list (sims_pre_dict_list, additional dict_keys: ["TI"], ["AU"], ["SO"], ["AB"], ["MH"]).
    Furthermore, ["MH"] is structurally corrected to MeSH (additional key = ["MeSH"]) and individual
    "stickyness" (number of similar articles in PubMed) is added (key = ["STCK"]).
    Relies on Biopython (Entrez, Medline), time (sleep), MH2MeSH(dictionary) and a former run of the function 'sims4pmid()'.
    Is slow but reliable!"""
    
    from Bio import Entrez, Medline
    from time import sleep
    #
    j = range(25, 501, 25)
    i = 0
    for item in sims_pre_dict_list:
        sim_pmid = item["Id"]
        sleep(.3)
        sim_record = list(Medline.parse(Entrez.efetch(db="pubmed", id=sim_pmid, rettype="medline", retmode="text")))
    #
        b_record = Entrez.read(Entrez.elink(db="pubmed", id=sim_pmid, cmd="neighbor_score"))
        if b_record[0]["LinkSetDb"][0]["Link"]:
            item["STCK"] = len(b_record[0]["LinkSetDb"][0]["Link"])
        del b_record
    #
        if "TI" in sim_record[0]:
            item["TI"] = sim_record[0]["TI"]
        else:
            item["TI"] = ""
        if "AU" in sim_record[0]:
            item["AU"] = sim_record[0]["AU"]
        else:
            item["AU"] = ""   
        if "SO" in sim_record[0]:
            item["SO"] = sim_record[0]["SO"]
        else:
            item["SO"] = ""
        if "AB" in sim_record[0]:
            item["AB"] = sim_record[0]["AB"]
        else:
            item["AB"] = ""
        if "MH" in sim_record[0]:
            item["MH"] = sim_record[0]["MH"]
        else:
            item["MH"] = []
    #    
        del sim_record
    #
        MH2MeSH(item)
    #
        i += 1
        if i in j:
            print("\t", i, "observations are complete ...")
        elif i == len(sims_pre_dict_list):
            print("\33[34m" + "\n\tAll relevant data is available!" + "\33[0m")
        else:
            continue
#
#
def positives_where(dict_list, pos):
    """..."""
    #
    import pandas as pd
    #
    pos_positions = []
    x = 1 # Position 1 at PubMed is Reference document, Similars start at position 2.
    for elem in dict_list:
        x += 1
        y, z = elem["Id"], int(elem["Score"])
        if y in pos:
            pos_positions.append((x, y, z))
    #
    pos_positions_df = pd.DataFrame(pos_positions, columns=('Ranking', 'PMID', 'Score')).set_index('PMID')
    #
    if pos_positions:
        print("\033[92m" + "\nPositives are present in Similar articles!" + "\033[0m")
        print("\033[34m" + "\nNative PubMed Ranking and Similarity Scores:" + "\033[0m")
        #print("\033[34m" + "Ranking of positive controls:\n" + "\033[0m")
        #print("PMID","\t\tPubMed-Rank", "\tPubMed-Score")
        #for elem in sorted(pos_positions):
            #print(elem[1], "\t", elem[0], "\t\t", elem[2])
        #print("\n")
    else:
        print("\n\033[91m" + "No positive controls were entered (or identified in PubMed respectively)!" + "\033[0m")
    #
    return pos_positions, pos_positions_df
#
#
def sims_scores2fig(pmid, sims_count, sims_dict_list, pos_positions=[]):
    """Returns a MatPlotLib-Diagramm (fig) of the original Similarity Scores as provided by PubMed (sims_scores).
    Requires the PMID of the reference article (pmid) and the number of similar articles (sims_count).
    Furthermore, depicts the positions of the positives indicated by the user (pos_positions).
    Relies on numpy (imported as np) and matplotlib.pyplot (imported as plt)."""
    
    import numpy as np
    import matplotlib.pyplot as plt
    #
    sims_scores = [int(item["Score"]) for item in sims_dict_list]
    #
    sim = np.array(sims_scores)
    x = range(1, sims_count+1)
    if pos_positions:
        x2 = []
        posi =[]
        for item in pos_positions:
            down = item[0]
            up = item[2]
            x2.append(down)
            posi.append(up)
    fig = plt.figure()
    ax = plt.axes()
    ax.set_xlim([0, sims_count+1])
    ax.set_ylim([0, 1.2e8])
    ax.set_xticks(range(0, sims_count+1, 25))
    ax.set_yticks([0.1e8, 0.2e8, 0.3e8, 0.4e8, 0.5e8, 0.6e8, 0.7e8, 0.8e8, 0.9e8, 1.0e8, 1.1e8, 1.2e8])
    ax.grid()
    plt.plot(x, sim, ".g")
    if pos_positions:
        plt.plot(x2, posi, "or")
    plt.title("Native Scores of Similar Articles (green), Positives (red)\n")
    plt.xlabel("PubMed Ranking")
    plt.ylabel("Similarity Score x 1e^8")
    style = dict(size=10, color="black")
    ax.text(1, 1.13e8, "PMID:", **style)
    ax.text(26, 1.13e8, pmid, **style)
    ax.text(1, 1.03e8, "similar:", **style)
    ax.text(26, 1.03e8, sims_count, **style)
    #
    return fig
#
#
def score2kali_A_B(ref_meta, sims_meta):
    """Calibration of the PubMed similarity scores (key["Score"]) in 'sims_meta'-dictionaries.
    The score is influenced by the length of the abstract and the "stickyness" (i.e. the number of similar articles for a given pmid in PubMed).
    CAUTION: Calibration is only valid within certain thresholds.
    Therefore, numeric validity is checked. Otherwise, "0" is entered as value for new keys ["Kali_A"] and ["Kali_B"].
    Requires dictionaries with metadata of reference document ('ref_meta') and metadata of the respective similar article (item in 'sims_meta')."""
    
    import nltk_data
    from nltk_data_tokenize__init__ import word_tokenize
    import punkt, regexp, treebank
    #
    if ref_meta["AB"] and ref_meta["TI"]:
        ref_ab_ti2 = (ref_meta["AB"] + " " + ref_meta["TI"] + " " + ref_meta["TI"]) # Title is considered twice! (see PubMed_Help for deatils: ...)
        ref_length = len(word_tokenize(ref_ab_ti2.lower()))
    else:
        ref_length = 0
    #
    ref_stickyness = int(ref_meta["STCK"])
    #
    for item in sims_meta:
        sim_score = int(item["Score"])
        sim_stickyness = int(item["STCK"])
        if item["AB"] and item["TI"]:
            sim_ab_ti2 = (item["AB"] + " " + item["TI"] + " " + item["TI"])
            sim_length = len(word_tokenize(sim_ab_ti2.lower()))
        else:
            sim_length = 0
    #
        if ref_stickyness > 500 or ref_stickyness == 0:
            item["Kali_A"], item["Kali_B"] = 0, 0
        elif sim_stickyness > 500 or sim_stickyness == 0:
            item["Kali_A"], item["Kali_B"] = 0, 0 
        elif ref_length  > 500 or ref_length == 0:
            item["Kali_A"], item["Kali_B"] = 0, 0
        elif sim_length > 500 or sim_length == 0:
            item["Kali_A"], item["Kali_B"] = 0, 0
        else:
            item["Kali_A"] = (sim_score - (ref_stickyness * 139810) - (((ref_length + sim_length)//2) * 127196)) + 52500000
            item["Kali_B"] = (sim_score - (((ref_stickyness + sim_stickyness)//2) * 139810) - (((ref_length + sim_length)//2) * 127196)) + 52500000
    #
    del ref_ab_ti2, ref_length, ref_stickyness, sim_score, sim_stickyness, sim_ab_ti2, sim_length
#
#
def kali_positives_where(sims_dict_list, positives, kali='A'):
    # ACHTUNG! kali_(pos)_positions/ranking Reihenfolge: Rang, PMID, Score
    if kali == 'A':
        kali_scores = [(int(elem["Kali_A"])) for elem in sims_dict_list]
        #
        kali_pos_positions = []
        x = 0
        for elem in sims_dict_list:
            x += 1
            y, z = elem["Id"], int(elem["Kali_A"])
            if y in positives:
                kali_pos_positions.append((x, y, z))
        #
        kali_A_ranking_prae = [(int(elem["Kali_A"]), elem["Id"]) for elem in sims_dict_list]
        temp = list(enumerate(reversed(sorted(kali_A_ranking_prae))))
        kali_ranking = []
        for item in temp:
            u = int(item[0]) + 1
            v = item[1][1]
            w = int(item[1][0])
            kali_ranking.append((u, v, w))
        del kali_A_ranking_prae, temp
    #
    elif kali == 'B':
        kali_scores = [(int(elem["Kali_B"])) for elem in sims_dict_list]
        #
        kali_pos_positions = []
        x = 0
        for elem in sims_dict_list:
            x += 1
            y, z = elem["Id"], int(elem["Kali_B"])
            if y in positives:
                kali_pos_positions.append((x, y, z))
        #
        kali_B_ranking_prae = [(int(elem["Kali_B"]), elem["Id"]) for elem in sims_dict_list]
        temp = list(enumerate(reversed(sorted(kali_B_ranking_prae))))
        kali_ranking = []
        for item in temp:
            u = int(item[0]) + 1
            v = item[1][1]
            w = int(item[1][0])
            kali_ranking.append((u, v, w))
        del kali_B_ranking_prae, temp
    #
    if kali_pos_positions:
        print("\033[34m" + "\nAlternative", kali, ": Ranking of positive controls\n" + "\033[0m")
        print("PMID","\t\t", kali, "Rank", "\t", kali, "Score")
        for item in kali_ranking:
            if item[1] in positives:
                print(item[1], "\t", item[0], "\t\t", item[2])
        print("\n")
    else:
        print("\n\033[91m" + "No positive controls were entered (or identified in PubMed respectively)!" + "\033[0m")
    #
    return kali_ranking
#
#
def create_thresh(sims_count, level):
    """Generates a \"sims_count\" number of equal (level) values."""
    thresh = []
    f = 0
    while f <= sims_count:
        f += 1
        thresh.append(level)
        if f == sims_count:
            break
    #        
    return thresh
#
#
def kali_scores2fig(pmid, sims_count, sims_dict_list, positives, kali='A'):
    """Returns a MatPlotLib-Diagramm (fig) of the original Similarity Scores as provided by PubMed (sim_scores).
    Requires the PMID of the reference article (pmid) and the number of similar articles (ref_stickyness).
    Relies on numpy (imported as np) and matplotlib.pyplot (imported as plt)."""
    
    import numpy as np
    import matplotlib.pyplot as plt
    #
    sims_scores = [int(elem["Score"]) for elem in sims_dict_list]
    #
    if kali == 'A':
        kali_scores = [(int(elem["Kali_A"])) for elem in sims_dict_list]
        kali_pos_positions = []
        x = 0
        for elem in sims_dict_list:
            x += 1
            y, z = elem["Id"], int(elem["Kali_A"])
            if y in positives:
                kali_pos_positions.append((x, y, z))
    elif kali == 'B':
        kali_scores = [(int(elem["Kali_B"])) for elem in sims_dict_list]
        kali_pos_positions = []
        x = 0
        for elem in sims_dict_list:
            x += 1
            y, z = elem["Id"], int(elem["Kali_B"])
            if y in positives:
                kali_pos_positions.append((x, y, z))
    #
    zero_values = 0
    for item in sims_dict_list:
        if item["Kali_A"] == 0:
            zero_values += 1   
    #
    sim = np.array(sims_scores)
    kali = np.array(kali_scores)
    if kali_pos_positions:
        x3 = []
        posi3 =[]
        for item in kali_pos_positions:
            down = item[0]
            up = item[2]
            x3.append(down)
            posi3.append(up)
    thresh05 = create_thresh(sims_count, 0.5e8)
    thresh06 = create_thresh(sims_count, 0.6e8)
    thresh07 = create_thresh(sims_count, 0.7e8)
    thresh08 = create_thresh(sims_count, 0.8e8)
    thresh09 = create_thresh(sims_count, 0.9e8)
    thresh10 = create_thresh(sims_count, 1.0e8)
    thresh11 = create_thresh(sims_count, 1.1e8)
    thresh12 = create_thresh(sims_count, 1.2e8)
    x = range(1, sims_count+1)
    fig = plt.figure()
    ax = plt.axes()
    ax.set_xlim([0, sims_count+1])
    ax.set_ylim([0, 1.2e8])
    ax.set_xticks(range(0, sims_count+1, 25))
    ax.set_yticks([0.1e8, 0.2e8, 0.3e8, 0.4e8, 0.5e8, 0.6e8, 0.7e8, 0.8e8, 0.9e8, 1.0e8, 1.1e8, 1.2e8])
    ax.grid()
    plt.plot(x, sim, ":g")
    plt.plot(x, kali, ".g")
    if kali_pos_positions:
        plt.plot(x3, posi3, "or")
    plt.fill_between(x, thresh05, thresh06, color="yellow", alpha=0.2)
    plt.fill_between(x, thresh06, thresh07, color="yellow", alpha=0.3)
    plt.fill_between(x, thresh07, thresh08, color="orange", alpha=0.4)
    plt.fill_between(x, thresh08, thresh09, color="orange", alpha=0.6)
    plt.fill_between(x, thresh09, thresh10, color="orange", alpha=0.8)
    plt.fill_between(x, thresh10, thresh11, color="red", alpha=0.5)
    plt.fill_between(x, thresh11, thresh12, color="red", alpha=0.6)
    plt.title("Kalibrierte Scores der Similar Articles (grün) und Positivkontrollen (rot)\n")
    plt.xlabel("Position in der PubMed-Trefferliste")
    plt.ylabel("Calibrated Score x 1e^8")
    style = dict(size=10, color="black")
    ax.text(1, 1.13e8, "PMID:", **style)
    ax.text(26, 1.13e8, pmid, **style)
    ax.text(1, 1.03e8, "similar:", **style)
    ax.text(26, 1.03e8, sims_count, **style)
    ax.text(1, .93e8, "outlier:", **style)
    ax.text(26, .93e8, zero_values, **style)
    #
    return fig
#
#
def choose_rerank(positives, sims_meta, number_results):
    if positives:
        valid = ["a", "b"]
        i = 0
        choice = "z"
        while choice.lower() not in valid:
            i += 1
            choice = input("\nPlease CHOOSE one of the Alternative Rankings. A or B? ")
            if i == 4:
                raise KeyboardInterrupt
        i_rank = []
        if choice.lower() == "a":
            tmp_rank_a = sims_meta['ARank'].sort_values(kind='mergesort', ascending=True).index[:number_results]
            i_rank = [elem for elem in tmp_rank_a]
        elif choice.lower() == "b":
            tmp_rank_b = sims_meta['BRank'].sort_values(kind='mergesort', ascending=True).index[:number_results]
            i_rank = [elem for elem in tmp_rank_b]
    else:
        print("\033[91m" + "\nInteractive re-ranking is not possible, if no positves were entered!" + "\033[0m")
        raise KeyboardInterrupt
    #
    return choice, i_rank
#
#
def out4pubmed(choice, ranking):
    temp = []
    if ranking != []:
        for item in ranking:
            temp.append(item)
            temp.append("+OR+")
    temp = "".join(temp)
    out = temp[:-5]
    print("\n\033[94m" + "Reranking:"+ "\033[0m", choice.upper())
    #print("\n\033[94m" + "PubMed-Input: Please transfer with 'copy&paste'!" + "\033[0m", end = "\n\n")
    #print(out)
    #
    return out
#
#
#Der folgende Code ist übernommen von Prateek Joshi, "Artificial Intelligence with Python", 2017, Packt Publishing
#
def text2stem_tokens_list(text):
    """Processes an input text and returns a list of stemmed tokens where stop words are excluded."""
    
    import nltk_data, nltk_data_tokenize__init__, nltk_data_stem__init__
    from nltk_data_corpus__init__ import stopwords
    from regexp import RegexpTokenizer
    from snowball import SnowballStemmer
    #
    #Create a regular expression tokenizer
    tokenizer = RegexpTokenizer(r'\w+')
    #
    #Create a Snowball stemmer
    stemmer = SnowballStemmer('english')
    #
    #Get the list of stop words
    stop_words = stopwords.words('english')
    #
    #Tokenize the input string
    tokens = tokenizer.tokenize(text.lower())
    #
    #Remove the stop words
    tokens = [x for x in tokens if not x in stop_words]
    #
    #Perform stemming on the tokenized words
    tokens_stemmed = [stemmer.stem(x) for x in tokens]
    #
    return tokens_stemmed
#
#
def tfidf_scores2fig(pmid, sims_count, sims_scores, tfidf_scores, tfidf_zeros, tfidf_pos_positions):
    """Returns a MatPlotLib-Diagramm (fig) of the original Similarity Scores as provided by PubMed (sim_scores) and the TFIDF-Scores.
    Requires the PMID of the reference article (pmid), the number of similar articles (sims_count), the PubMed Similarity Scores (sims_scores),
    the TFIDF-Scores (tfidf_scores) and the number of zero values (tfidf_zeros). CAUTION: Keep an eye on the differences in units and adjust!
    Relies on numpy (imported as np) and matplotlib.pyplot (imported as plt)."""
    
    import numpy as np
    import matplotlib.pyplot as plt
    #
    sim = np.array(sims_scores)
    tfidf = np.array(tfidf_scores)
    if tfidf_pos_positions:
        x4 = []
        posi4 =[]
        for item in tfidf_pos_positions:
            down = item[0]
            up = item[2]
            x4.append(down)
            posi4.append(up)
    thresh10 = create_thresh(sims_count, 1.0e8)
    x = range(1, sims_count+1)
    fig = plt.figure()
    ax = plt.axes()
    ax.set_xlim([0, sims_count+1])
    ax.set_ylim([0, 1.2e8])
    ax.set_xticks(range(0, sims_count+1, 25))
    ax.set_yticks([0.1e8, 0.2e8, 0.3e8, 0.4e8, 0.5e8, 0.6e8, 0.7e8, 0.8e8, 0.9e8, 1.0e8, 1.1e8, 1.2e8])
    ax.grid()
    plt.plot(x, sim, ":g")
    plt.plot(x, tfidf, ".m")
    if tfidf_pos_positions:
        plt.plot(x4, posi4, "or")
    plt.plot(x, thresh10, "-r")
    plt.title("TFIDF Scores der Similar Articles (magenta) und Positivkontrollen (rot)\n")
    plt.xlabel("Position in der PubMed-Trefferliste")
    plt.ylabel("TFIDF-Similarity Score x 1e^8")
    style = dict(size=10, color="black")
    ax.text(1, 1.13e8, "PMID:", **style)
    ax.text(26, 1.13e8, pmid, **style)
    ax.text(1, 1.03e8, "similar:", **style)
    ax.text(26, 1.03e8, sims_count, **style)
    ax.text(1, .93e8, "outlier:", **style)
    ax.text(26, .93e8, tfidf_zeros, **style)
    
    return fig
#
#
def text2spacy_basetokens(text):
    """Processes an input text and returns a list of tokens in base forms (lemmas) where stop words are excluded."""
    
    import spacy
    #
    nlp = spacy.load("en_core_sci_sm")
    doc = nlp(text)
    #
    tokens = [token.lemma_.lower() for token in doc if not token.is_stop==True]
    #
    return tokens
#
#
def meta4sims_faster(sims_pre_dict_list):
    """Retrieval of more metadata  for the Similar Articles downloaded from PubMed with the function 'sims4pmid()'
    and entry into the respective dictionaries stored in a list (sims_pre_dict_list, additional dict_keys: ["TI"], ["AU"], ["SO"], ["AB"], ["MH"]).
    Furthermore, ["MH"] is structurally corrected to MeSH (additional key = ["MeSH"]) and individual
    "stickyness" (number of similar articles in PubMed) is added (key = ["STCK"]).
    Relies on Biopython (Entrez, Medline), time (sleep), MH2MeSH(dictionary) and a former run of the function 'sims4pmid()'."""
    
    from Bio import Entrez, Medline
    import time
    #
    Entrez.email = "..."
    #
    pmid_list = []
    for item in sims_pre_dict_list:
        pmid_list.append(int(item["Id"]))
        tmp = str(pmid_list)
    pmids = tmp[1:-1]
    # 
    session_history = Entrez.read(Entrez.esearch(db="pubmed", term=pmids, usehistory="y"))
    #
    webenv = session_history["WebEnv"]
    qkey = session_history["QueryKey"]
    count = session_history["Count"]
    #
    try:
        from urllib.error import HTTPError  # for Python 3
    except ImportError:
        from urllib2 import HTTPError  # for Python 2
    #
    records = []    
    count = int(count)
    batch_size = 100
    for start in range(0, count, batch_size):
        end = min(count, start+batch_size)
        #print("Datensätze %i bis %i werden heruntergeladen ..." % (start+1, end))
        attempt = 0
        while attempt < 3:
            attempt += 1
            try:
                fetch_handle = Entrez.efetch(db="pubmed",rettype="medline",retmode="text",
                                             retstart=start,
                                             retmax=batch_size,
                                             WebEnv=webenv,
                                             query_key=qkey)
            except HTTPError as err:
                if 500 <= err.code <= 599:
                    print("Received error from server %s" % err)
                    print("Attempt %i of 3" % attempt)
                    attempt += 1
                    time.sleep(15)
                else:
                    raise
        records_batch = Medline.parse(fetch_handle)
        records_batch = list(records_batch)
        records += records_batch
        fetch_handle.close()
        del fetch_handle
    #
    for item in sims_pre_dict_list:
        for jitem in records:
            if item["Id"] == jitem["PMID"]:
                if "TI" in jitem:
                    item["TI"] = jitem["TI"]
                else:
                    item["TI"] = ""
                if "AU" in jitem:
                    item["AU"] = jitem["AU"]
                else:
                    item["AU"] = []   
                if "SO" in jitem:
                    item["SO"] = jitem["SO"]
                else:
                    item["SO"] = ""
                if "AB" in jitem:
                    item["AB"] = jitem["AB"]
                else:
                    item["AB"] = ""
                if "MH" in jitem:
                    item["MH"] = jitem["MH"]
                else:
                    item["MH"] = []
                #
                MH2MeSH(item)

    #
    del records
    #
    i = range(0, count+1, 25)
    j = 0
    print("\n\033[34m" + "Completed records:" + "\033[0m", end=" ")
    for item in sims_pre_dict_list:
        time.sleep(0.1)
        j += 1
        b_record = Entrez.read(Entrez.elink(db="pubmed", id=item["Id"], cmd="neighbor_score"))
        if b_record[0]["LinkSetDb"] != []:
            if b_record[0]["LinkSetDb"][0]["Link"]:
                item["STCK"] = len(b_record[0]["LinkSetDb"][0]["Link"])
            else:
                item["STCK"] = 0
        else:
            item["STCK"] = 0
        #
        del b_record
        #
        if j in i:
            print(j, end=", ")
    #
    final = count - 1
    if sims_pre_dict_list[final]["STCK"]:
        print("\33[92m" + "\n\n\tAll relevant data is available!" + "\33[0m")
    else:
        print("\33[31m" + "\n\tThere is a problem!" + "\33[0m")
#
#
def meta4pmids(pmids):
    """Retrieval of metadata  for PMIDs (type = string, i.e. "12345678, 90123456") from PubMed
    and entry into dictionaries stored in a list (meta_dict_list, additional dict_keys: ["TI"], ["AU"], ["SO"], ["AB"], ["MH"]).
    Furthermore, ["MH"] is structurally corrected to MeSH (additional key = ["MeSH"]) and individual
    "stickyness" (number of similar articles in PubMed) is added (key = ["STCK"]).
    Relies on Biopython (Entrez, Medline), time (sleep), MH2MeSH(dictionary)."""
    
    from Bio import Entrez, Medline
    import time
    #
    Entrez.email = "..."
    # 
    session_history = Entrez.read(Entrez.esearch(db="pubmed", term=pmids, usehistory="y"))
    #
    webenv = session_history["WebEnv"]
    qkey = session_history["QueryKey"]
    count = session_history["Count"]
    #
    try:
        from urllib.error import HTTPError  # for Python 3
    except ImportError:
        from urllib2 import HTTPError  # for Python 2
    #
    records = []    
    count = int(count)
    batch_size = 100
    for start in range(0, count, batch_size):
        end = min(count, start+batch_size)
        #print("Datensätze %i bis %i werden heruntergeladen ..." % (start+1, end))
        attempt = 0
        while attempt < 3:
            attempt += 1
            try:
                fetch_handle = Entrez.efetch(db="pubmed",rettype="medline",retmode="text",
                                             retstart=start,
                                             retmax=batch_size,
                                             WebEnv=webenv,
                                             query_key=qkey)
            except HTTPError as err:
                if 500 <= err.code <= 599:
                    print("Received error from server %s" % err)
                    print("Attempt %i of 3" % attempt)
                    attempt += 1
                    time.sleep(15)
                else:
                    raise
        records_batch = Medline.parse(fetch_handle)
        records_batch = list(records_batch)
        records += records_batch
        fetch_handle.close()
        del fetch_handle
    #
    meta_dict_list = [{"Id": entry} for entry in list(pmids.split(", "))]
    #
    for item in meta_dict_list:
        for jitem in records:
            if item["Id"] == jitem["PMID"]:
                if "TI" in jitem:
                    item["TI"] = jitem["TI"]
                else:
                    item["TI"] = ""
                if "AU" in jitem:
                    item["AU"] = jitem["AU"]
                else:
                    item["AU"] = []   
                if "SO" in jitem:
                    item["SO"] = jitem["SO"]
                else:
                    item["SO"] = ""
                if "AB" in jitem:
                    item["AB"] = jitem["AB"]
                else:
                    item["AB"] = ""
                if "MH" in jitem:
                    item["MH"] = jitem["MH"]
                else:
                    item["MH"] = []
                #
                MH2MeSH(item)

    #
    del records
    #
    i = range(0, count+1, 25)
    j = 0
    for item in meta_dict_list:
        time.sleep(0.1)
        j += 1
        b_record = Entrez.read(Entrez.elink(db="pubmed", id=item["Id"], cmd="neighbor_score"))
        if b_record[0]["LinkSetDb"] != []:
            if b_record[0]["LinkSetDb"][0]["Link"]:
                item["STCK"] = len(b_record[0]["LinkSetDb"][0]["Link"])
                item["TopSim"] = b_record[0]["LinkSetDb"][0]["Link"][0]
            else:
                item["STCK"] = 0
                item["TopSim"] = {}
        else:
            item["STCK"] = 0
            item["TopSim"] = {}
        #
        del b_record
        #
        if j in i:
            print(j, "Datensätze wurden vervollständigt ...")
    #
    final = count - 1
    if meta_dict_list[final]["STCK"]:
        print("\33[34m" + "\n\tAlle relevanten Daten stehen zur Verfügung!" + "\33[0m")
    else:
        print("\33[31m" + "\n\tEs gibt ein Problem!" + "\33[0m")
    #
    return meta_dict_list
#
#
def MH2Headings_norm(dictionary):
    """Strips complex MeSH-Subheadings-Combinations to pure headings.
    Normalizes headings to normal terms (i.e. "Research, Biomedical" to "Biomedical Research"). Returns list of headings."""
    
    if dictionary["MH"]:
        headings = []
        for entry in dictionary["MH"]:
            term0 = entry.replace("*", "")
            sep1 = term0.find(",")
            sep2 = term0.find("/")
            if sep1 == -1 and sep2 == -1:
                headings.append(term0)
            elif sep1 == -1 and sep2 != -1:
                head0 = term0[:sep2]
                headings.append(head0)
            elif sep1 != -1 and sep2 == -1:
                first = term0[sep1+2:]
                second = term0[:sep1]
                head1 = first + " " +second
                headings.append(head1)
            elif sep1 != -1 and sep2 != -1:
                first = term0[sep1+2:sep2]
                second = term0[:sep1]
                head2 = first + " " + second
                headings.append(head2)
            else:
                print("Something went wrong!")
    else:
        headings = []
    #
    return headings
#
#
def MH2Headings(dictionary):
    """Strips complex MeSH-Subheadings-Combinations to pure headings. Returns list of headings."""
    
    if dictionary["MH"]:
        headings = []
        for entry in dictionary["MH"]:
            term = entry.replace("*", "")
            sep = term.find("/")
            if sep == -1:
                headings.append(term)
            elif sep != -1:
                head = term[:sep]
                headings.append(head)
            else:
                print("Something went wrong!")
    else:
        headings = []
    #
    return headings
#
#
def query2pmids(query):
    """Returns a string of comma-separated sorted pmids from a query-string, default database is PubMed/MEDLINE.
    Also returns the number of results for a given query (count), maximum is set to 10.000.
    Query-string-syntax like this:
    "\"duplicate publication\"[pt] OR \"corrected and republished article\"[pt]"."""
    
    from Bio import Entrez
    #
    Entrez.email = "..."
    #
    session_history = Entrez.read(Entrez.esearch(db="pubmed", term=query, usehistory="y"))
    #
    webenv = session_history["WebEnv"]
    qkey = session_history["QueryKey"]
    count = session_history["Count"]
    #
    record = Entrez.read(Entrez.esearch(db="pubmed", term="", query_key=qkey, WebEnv=webenv, retmax=10000)) 
    #
    tmp = [int(element) for element in record["IdList"]]
    #
    pmids = str(sorted(tmp))
    #
    pmids4pubmed = pmids[1:-1]
    #
    return count, pmids4pubmed
#
#
def sims_dict2pmids(sims_dict):
    """Returns a string of comma-separated PMIDs from a pandasSeries containing "dicts" with ['Id'] ..."""
    pmid_list = []
    for item in sims_dict:
        pmid_list.append(int(item["Id"]))
        tmp = str(pmid_list)
    pmids = tmp[1:-1]
    return pmids
#
#
def meta4pmids_pandas(pmids):
    """Retrieval of metadata  for PMIDs (type = string, i.e. "12345678, 90123456") from PubMed
    and entry into dictionaries stored in a list (meta_dict_list, additional dict_keys: ["TI"], ["AU"], ["SO"], ["AB"], ["MH"]).
    Furthermore, ["MH"] is structurally corrected to MeSH (additional key = ["MeSH"]) and individual
    "stickyness" (number of similar articles in PubMed) is added (key = ["STCK"]).
    Relies on Biopython (Entrez, Medline), time (sleep), lola.MH2MeSH(dictionary), numpy, pandas."""
    
    from Bio import Entrez, Medline
    import pylolaos as lola
    #
    Entrez.email = "..."
    Entrez.tool = "GSPsearch"
    Entrez.api_key = "cc1675df78cd50d9a100748cd79a6ab6c808"
    # 
    session_history = Entrez.read(Entrez.esearch(db="pubmed", term=pmids, usehistory="y"))
    #
    webenv = session_history["WebEnv"]
    qkey = session_history["QueryKey"]
    count = session_history["Count"]
    #
    from urllib.error import HTTPError
    import time
    #
    records = []    
    count = int(count)
    batch_size = 100
    for start in range(0, count, batch_size):
        end = min(count, start+batch_size)
        attempt = 0
        while attempt < 3:
            attempt += 1
            try:
                fetch_handle = Entrez.efetch(db="pubmed",rettype="medline",retmode="text",
                                             retstart=start,
                                             retmax=batch_size,
                                             WebEnv=webenv,
                                             query_key=qkey)
            except HTTPError as err:
                if 500 <= err.code <= 599:
                    print("Received error from server %s" % err)
                    print("Attempt %i of 3" % attempt)
                    attempt += 1
                    time.sleep(15)
                else:
                    raise 
        records_batch = list(Medline.parse(fetch_handle))
        records += records_batch
        fetch_handle.close()
        del fetch_handle
        del records_batch
    #
    for elem in records:
        lola.MH2MeSH(elem)
    #
    import numpy as np
    import pandas as pd
    #
    meta_series_dict_I = {elem["PMID"]: pd.Series(elem, index=["TI", "AU", "SO", "AB", "MH", "MeSH"]) for elem in records}    
    #
    meta_df_I = pd.DataFrame(meta_series_dict_I).transpose()
    #
    del records
    #
    i = range(0, count+1, 25)
    j = 0
    meta_dicts_list = []
    print("\n\033[34m" + "Completed records:" + "\033[0m", end=" ")
    for elem in list(pmids.split(", ")):
        #time.sleep(0.1)
        tmp = {}
        j += 1
        b_record = Entrez.read(Entrez.elink(db="pubmed", id=elem, cmd="neighbor_score", api_key="cc1675df78cd50d9a100748cd79a6ab6c808"))
        tmp["PMID"] = b_record[0]["IdList"][0]
        if b_record[0]["LinkSetDb"] != []:
            if b_record[0]["LinkSetDb"][0]["Link"]:
                tmp["STCK"] = len(b_record[0]["LinkSetDb"][0]["Link"])
                tmp["TopSimId"] = b_record[0]["LinkSetDb"][0]["Link"][0]["Id"]
                tmp["TopSimScore"] = int(b_record[0]["LinkSetDb"][0]["Link"][0]["Score"])
            else:
                tmp["STCK"] = 0
                tmp["TopSimId"] = ""
                tmp["TopSimScore"] = 0
        else:
            tmp["STCK"] = 0
            tmp["TopSimId"] = ""
            tmp["TopSimScore"] = 0
        #
        meta_dicts_list.append(tmp)
        #
        del b_record
        #
        if j in i:
            print(j, end=", ")
    #
    final = count - 1
    if meta_dicts_list[final]["STCK"]:
        print("\33[92m" + "\n\n\tAll relevant data is available!" + "\33[0m")
    else:
        print("\33[31m" + "\n\tThere is a problem!" + "\33[0m")
#
    #
    meta_df_II = pd.DataFrame(meta_dicts_list).set_index("PMID")
    #
    meta_df = meta_df_I.join(meta_df_II)
    #
    
    #
    return meta_df
#
#
def meta4pmid_in_pandas():
    """Retrieval of metadata for an inputted PMID from PubMed.
    Returns pmid and corrected metadata in a pandasSeries (ref_ser).
    If no valid PMID is entered ref_dict contains an error message as the only value (key = ["id:"]).
    Entry of a valid PMID is repeated then (2 times).
    Furthermore, ref_dict["MH"] is structurally corrected to MeSH (new key = ["MeSH"])
    by running function 'MH2MeSH(ref_dict)' and
    "stickyness" (number of similar articles in PubMed) is added (key = ["STCK"]).
    Relies on pandas, Biopython (Entrez, Medline) and function 'MH2MeSH(dictionary)'."""
    
    from Bio import Entrez, Medline
    import pandas as pd
    #
    Entrez.email = "..."
    Entrez.tool = "GSPsearch"
    Entrez.api_key = "cc1675df78cd50d9a100748cd79a6ab6c808"
    #
    ref_dict = {"id:": ""}
    q = 0
    while "id:" in ref_dict:
        q += 1
        pmid = input("\n\tPlease ENTER a valid PMID: ")
        record = list(Medline.parse(Entrez.efetch(db="pubmed", id=pmid, rettype="medline", retmode="text")))
        ref_dict = record[0]
        del record[1:]
        if q == 4:
            print("\33[91m" + "\n\nPlease REFER TO PubMed for a valid PMID!" + "\33[0m\n")
            raise KeyboardInterrupt
    #    
    b_record = Entrez.read(Entrez.elink(db="pubmed", id=pmid, cmd="neighbor_score"))
    if b_record[0]["LinkSetDb"][0]["Link"]:
        ref_dict["STCK"] = len(b_record[0]["LinkSetDb"][0]["Link"])
        del b_record
    else:
        ref_dict["STCK"] = 0
    #
    if "TI" not in ref_dict:
        ref_dict["TI"] = ""
    if "AB" not in ref_dict:
        ref_dict["AB"] = ""
    #
    MH2MeSH(ref_dict)
    #
    ref_ser = pd.Series(ref_dict, index=['PMID', 'TI', 'AU', 'SO', 'AB', 'MH', 'MeSH', 'STCK'])
    del ref_dict
    #
    return pmid, ref_ser
#
#
def bodo_func(sims_pre_df, sims_scores_dict, pmid):
    """Function specific for GSPsearch"""
    import pandas as pd
    #
    sims_pre_df['PMID'] = sims_pre_df.index
    sims_scores_df = pd.DataFrame(sims_scores_dict)
    sims_scores_df['PMRank'] = sims_scores_df.index + 1
    sims_meta_df = pd.merge(sims_pre_df, sims_scores_df, left_on='PMID',right_on='Id').set_index('PMID')
    sims_meta_df = sims_meta_df.rename(columns={'Id':'RefId', 'Score':'RefScore'})
    sims_meta_df['RefId'] = pmid
    #
    print("\33[92m" + "\n\tEverything is fine!" + "\33[0m" + "\n")
    #
    input("Please press 'ENTER' to continue ...\n")
    #
    return sims_meta_df
#
#
def calibr8(sims_meta, sims_count, ref_length):
    #
    import pandas as pd
    import nltk_data
    from nltk_data_tokenize__init__ import word_tokenize
    #
    hasTI = sims_meta['TI'].notnull()
    hasAB = sims_meta['AB'].notnull()
    sims_meta_tiab = sims_meta[hasTI & hasAB]
    #
    sims_meta['tokTI'] = sims_meta_tiab.apply({'TI': word_tokenize})
    sims_meta['tokAB'] = sims_meta_tiab.apply({'AB': word_tokenize})
    #
    hastokTI = sims_meta['tokTI'].notnull()
    hastokAB = sims_meta['tokAB'].notnull()
    sims_meta_toktiab = sims_meta[hastokTI & hastokAB]
    #
    sims_meta['lenTI'] = sims_meta_toktiab.apply({'tokTI': [len]})
    sims_meta['lenAB'] = sims_meta_toktiab.apply({'tokAB': [len]})
    #
    sims_meta['Length'] = sims_meta['lenTI'].fillna(0) + sims_meta['lenTI'].fillna(0) + sims_meta['lenAB'].fillna(0)
    #
    sims_meta['cScore_A'] = (sims_meta['RefScore'] - (sims_count * 139810) - \
                            (((ref_length + sims_meta['Length'].astype(int))//2) * 127196)) + 52500000
    #
    sims_meta['cScore_B'] = (sims_meta['RefScore'] - (((sims_count  + sims_meta['STCK'])//2) * 139810) - \
                            (((ref_length + sims_meta['Length'].astype(int))//2) * 127196)) + 52500000
    #
    outSTCK = (sims_meta['STCK'] > 500) ^ (sims_meta['STCK'] == 0)
    outLength = (sims_meta['Length'] > 500) ^ (sims_meta['Length'] == 0)
    sims_out = sims_meta[outLength | outSTCK].index
    sims_meta.loc[sims_out, 'cScore_A'] = 0
    sims_meta.loc[sims_out, 'cScore_B'] = 0
    #
    del sims_meta['tokTI'], sims_meta['tokAB'], sims_meta['lenTI'], sims_meta['lenAB'], sims_meta['Length']
#
#
def cScores2ranking(sims_meta):
    #
    import pandas as pd
    #
    A_rank_ser = pd.Series(sims_meta.sort_values(['cScore_A'], ascending=False, kind='mergesort').index, name='PMIDs')
    #
    A_rank_df = pd.DataFrame(A_rank_ser)
    A_rank_df['ARank'] = A_rank_df.index + 1
    A_rank_df = A_rank_df.set_index('PMIDs')
    sims_meta = pd.merge(sims_meta, A_rank_df, left_index=True, right_index=True)
    #
    B_rank_ser = pd.Series(sims_meta.sort_values(['cScore_B'], ascending=False, kind='mergesort').index, name='PMIDs')
    #
    B_rank_df = pd.DataFrame(B_rank_ser)
    B_rank_df['BRank'] = B_rank_df.index + 1
    B_rank_df = B_rank_df.set_index('PMIDs')
    sims_meta = pd.merge(sims_meta, B_rank_df, left_index=True, right_index=True)
    #
    return sims_meta
#
#
def cPositives_where(positives, sims_meta):
    #
    import pandas as pd
    #
    positives_index = pd.Series(positives.split(', ')).values
    #
    PM_positions = sims_meta.loc[positives_index, 'PMRank'] + 1
    A_positions = sims_meta.loc[positives_index, 'ARank']
    B_positions = sims_meta.loc[positives_index, 'BRank']
    #
    tmp_dict = {'PubMed': PM_positions, 'Alternative_A': A_positions, 'Alternative_B': B_positions}
    #
    positions = pd.DataFrame(tmp_dict, columns=['PubMed', 'Alternative_A', 'Alternative_B']).sort_values('PubMed')
    #
    PM_scores = sims_meta.loc[positives_index, 'RefScore']
    A_scores = sims_meta.loc[positives_index, 'cScore_A']
    B_scores = sims_meta.loc[positives_index, 'cScore_B']
    #
    tmp_dict = {'PubMed': PM_scores, 'Alternative_A': A_scores, 'Alternative_B': B_scores}
    #
    scores = pd.DataFrame(tmp_dict, columns=['PubMed', 'Alternative_A', 'Alternative_B']).sort_values('PubMed',\
                                                                                                      ascending=False)
    #
    print("\033[34m" + "\nNative PubMed and Alternative Rankings:" + "\033[0m")
    #
    return positions, scores
#
#
def cScores2fig(pmid, sims_count, sims_meta, pos_positions, pos_scores, Alternative):
    """..."""
    #
    import matplotlib.pyplot as plt
    import pandas as pd
    #
    pmScores = sims_meta['RefScore'].sort_values(ascending=False, kind='mergesort')
    #
    if Alternative == 'A':
        cScores = sims_meta['cScore_A'].sort_values(ascending=False, kind='mergesort')
    elif Alternative == 'B':
        cScores = sims_meta['cScore_B'].sort_values(ascending=False, kind='mergesort')
    else:
        cScores = 0
    #
    if pos_positions.values.any():
        if Alternative == 'A':
            cPositions_pos = [x for x in pos_positions.values[:, 1]]
        elif Alternative == 'B':
            cPositions_pos = [x for x in pos_positions.values[:, 2]]
        else:
            cPositions_pos = []
    else:
        cPositions_pos = []
    #
    if pos_scores.values.any():
        if Alternative == 'A':
            cScores_pos = [x for x in pos_scores.values[:, 1]]
        elif Alternative == 'B':
            cScores_pos = [x for x in pos_scores.values[:, 2]]
        else:
            cScores_pos = []
    else:
        cScores_pos = []
    #
    x = range(1, sims_count+1)
    fig = plt.figure()
    ax = plt.axes()
    ax.set_xlim([0, sims_count+1])
    ax.set_ylim([0, 1.2e8])
    ax.set_xticks(range(0, sims_count+1, 25))
    ax.set_yticks([0.1e8, 0.2e8, 0.3e8, 0.4e8, 0.5e8, 0.6e8, 0.7e8, 0.8e8, 0.9e8, 1.0e8, 1.1e8, 1.2e8])
    ax.grid()
    #
    plt.plot(x, pmScores, ":g")
    plt.plot(x, cScores, ".g")
    plt.plot(cPositions_pos, cScores_pos, "or")
    #
    plt.title("Calibrated Scores of Similar Articles (big green), Positives (red)\n")
    plt.xlabel("Alternative Ranking")
    plt.ylabel("Calibrated Score x 1e^8")
    style = dict(size=10, color="black")
    ax.text(1, 1.13e8, "PMID:", **style)
    ax.text(26, 1.13e8, pmid, **style)
    ax.text(1, 1.03e8, "similar:", **style)
    ax.text(26, 1.03e8, sims_count, **style)
#    
    return fig
#
#
def no_vivo_pmids(sims_meta, i_rank):
#
    vivo_pmids = []
    for i in range(len(sims_meta['MH'])):
        if ('Animals' in MH2Headings(sims_meta.iloc[i]))\
        and not ('Drosophila melanogaster' in MH2Headings(sims_meta.iloc[i])\
                or 'Caenorhabditis elegans' in MH2Headings(sims_meta.iloc[i])):
            vivo_pmids.append(sims_meta['MH'].index[i])
#
    no_viv_pmids = []
    for elem in i_rank:
        if elem not in vivo_pmids:
            no_viv_pmids.append(elem)
            no_viv_pmids.append("+OR+")
#
    no_viv_pmids = "".join(no_viv_pmids)
    no_viv_pmids = no_viv_pmids[:-5]
#
    if no_viv_pmids == []:
        print("\33[91m" + "\n\nCannot identify alternatives!" + "\33[0m\n")
#
    return no_viv_pmids
#
#
def topic_cloud(pdseries, number):
#
    import matplotlib.pyplot as plt
    from wordcloud import WordCloud
#
    mesh = []
    for i in range(number):
        tmp_list = MH2Headings_norm(pdseries.iloc[i])
        for elem in tmp_list:
            elem = elem.replace(' ', '_')
            mesh.append(elem)
#
    wordcloud = WordCloud(background_color='white', \
                          max_words=20, \
                          min_font_size= 8, \
                          stopwords=None, \
                          width=250, \
                          height=150).generate(' '.join(mesh))
    plt.figure(figsize=(6, 6))
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.show()
#
#
def results2pubmed(results):
#
    from IPython.core.display import display, HTML
#
    link = "https://www.ncbi.nlm.nih.gov/pubmed/?term=" + results
    text = "<a href='{href}'>See your results at PubMed</a>"
    html = HTML(text.format(href=link))
#
    display(html)
#
#
def calibrate(sims_meta, ref_meta):
    #
    import pandas as pd
    from sklearn.linear_model import LinearRegression
    import pickle
    #
    hasTI = sims_meta['TI'].notnull()
    hasAB = sims_meta['AB'].notnull()
    sims_meta_tiab = sims_meta[hasTI & hasAB]
    #
    sims_meta['stemTI'] = sims_meta_tiab.apply({'TI': text2stem_tokens_list})
    sims_meta['stemAB'] = sims_meta_tiab.apply({'AB': text2stem_tokens_list})
    #
    hasstemTI = sims_meta['stemTI'].notnull()
    hasstemAB = sims_meta['stemAB'].notnull()
    sims_meta_stemtiab = sims_meta[hasstemTI & hasstemAB]
    #
    sims_meta['lenTI'] = sims_meta_stemtiab.apply({'stemTI': len})
    sims_meta['lenAB'] = sims_meta_stemtiab.apply({'stemAB': len})
    #
    hasMeSH = sims_meta['MeSH'].notnull()
    sims_meta['lenMeSH'] = sims_meta.apply({'MeSH': len})
    #
    data = sims_meta[['lenTI', 'lenAB', 'lenMeSH', 'RefScore', 'STCK']].fillna(0).astype('int')
    X = data.drop(['RefScore'], axis=1) 
    #
    with open('dup4cali_model_2021_9.pickle', 'rb') as file:
        trained_model = pickle.load(file)
    #
    sims_meta['sFactor'] = trained_model.predict(X)    
    #
    sims_meta['cScore_A'] = (sims_meta['RefScore'] / sims_meta['sFactor']) * 100000000
    #
    sims_meta['cScore_B'] = (sims_meta['RefScore'] / ((sims_meta['sFactor'] + ref_meta['rFactor'][0, 0]) / 2)) * 100000000
    #
    outSTCK = (sims_meta['STCK'] > 500) ^ (sims_meta['STCK'] == 0)
    outLength = ((sims_meta['lenTI'] + sims_meta['lenAB']) > 500) ^ ((sims_meta['lenTI'] + sims_meta['lenAB']) == 0)
    sims_out = sims_meta[outLength | outSTCK].index
    sims_meta.loc[sims_out, 'cScore_A'] = 0
    sims_meta.loc[sims_out, 'cScore_B'] = 0
    #
    del sims_meta['stemTI'], sims_meta['stemAB'], sims_meta['lenTI'], sims_meta['lenAB'], sims_meta['lenMeSH'], \
    sims_meta['sFactor']
#
#
def calibrate_ref(meta):
# meta has to be imported as pd.Series not dict!
    #
    import pandas as pd
    from sklearn.linear_model import LinearRegression
    import pickle
    #
    meta['stemTI'] = text2stem_tokens_list(meta['TI'])
    meta['stemAB'] = text2stem_tokens_list(meta['AB'])
    #
    meta['lenTI'] = len(meta['stemTI'])
    meta['lenAB'] = len(meta['stemAB'])
    #
    meta['lenMeSH'] = len(meta['MeSH'])
    meta['RefScore'] = 0
    #
    data = pd.DataFrame(meta[['lenTI', 'lenAB', 'lenMeSH', 'RefScore', 'STCK']])
    X = data.drop(['RefScore']).values.reshape(1, -1)
    #
    with open('dup4cali_model_2021_9.pickle', 'rb') as file:
        trained_model = pickle.load(file)
    
    meta['rFactor'] = trained_model.predict(X)
#
#
