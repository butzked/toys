# toys

Python 3 based tools for information retrieval from scientific resources like PubMed and subsequent processing of textual information.

Run Code in Jupyter-Notebooks at MyBinder!
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/butzked%2Ftoys/master)