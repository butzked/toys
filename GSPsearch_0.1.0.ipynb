{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "# __GSPsearch - Searching with Good Scientific Practice__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<div class='alert alert-block alert-info'>\n",
    "<p style=\"text-align:justify; font-size:15px;\">GSP in information retrieval requires TRANSPARENCY regarding employed search strategies, algorithms and filtering of results. Furthermore, individual searches should be WELL DOCUMENTED and REPRODUCIBLE. This notebook was elaborated to help users with transparently and reproducibly deciding, which of two provided algorithms is more adequate to retrieve relevant documents at the top positions of a search hit list.</p>\n",
    "<p style=\"text-align:justify; font-size:15px;\">The basic search hit list is retrieved from PubMed after specifying the searched-for contents with the help of a known reference citation. The hit list comprises citations which are considered 'similar' to the reference citation by a proprietary PubMed algorithm. You can enter the PMIDs (PubMed-Identifiers) of known relevant contents ('positives') and look up the positions of your positive controls within the basic hitlist.<b> KNOWLEDGE OF SUCH POSITIVES IS PREREQUISITE TO USING GSPsearch!</b> In addition, you can check the positions of your positives in two alternative rankings provided by GSPsearch. If ranked at anterior positions, it can be assumed that you will also find more relevant citations unknown to you at the topmost ranks of the hitlist.</p>\n",
    "<p style=\"text-align:justify; font-size:15px;\">The final output of the notebook is a list of 20 citations (PMIDs) that presumably are the most relevant citations in the hitlist retrieved from PubMed (> 30 million citations). <b>GSPsearch HAS TO BE RUN IN PARALLEL TO PUBMED, SINCE NO CITATION METADATA OTHER THAN PMIDs IS GENERATED!</b></p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "### __1. GSPsearch_Build__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<div class='alert alert-block alert-info'>\n",
    "<p style=\"text-align:justify; font-size:15px;\">The first section of the notebook provides all the necessary libraries and components to use GSPsearch.</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "import nltk\n",
    "nltk.download('punkt')\n",
    "import pylolaos_keep as lola; lola.go()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "### __2. GSPsearch_Retrieval__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<div class='alert alert-block alert-info'>\n",
    "<p style=\"text-align:justify; font-size:15px;\">This section compiles all the neccessary metadata from PubMed/MEDLINE to fuel the implemented algorithms. The following steps are required:<br>1.) Enter the PMID of your reference (e.g. 27234907),<br> 2.) Confirm the retrieved citation,<br> 3.) Enter your positives (known relevant documents, e.g. 31189558, 29636734, 29090680, 26654218).</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "__Enter reference document__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "pmid, ref_meta = lola.meta4pmid_in_pandas(); print(\"\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "__Check reference metadata__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "lola.print_biblio(ref_meta)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "__Confirm reference__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "lola.confirm_ref()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "__Internal checks of reference__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "sims_count, sims_scores_dict = lola.sims4pmid(pmid); ref_length = lola.outlier_ref(sims_count, ref_meta)\n",
    "print(\"\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "__Enter known relevant articles (`positives´)__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "positives = lola.get_pos()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<div class='alert alert-block alert-info'>\n",
    "<p style=\"text-align:justify; font-size:15px;\">In the cells below, some numerical data of the search hitlist retrieved from PubMed/MEDLINE is presented. The hitlist comprises citations which are considered 'similar' to your reference by a proprietary PubMed algorithm. The hitlist in its 'native' state is ranked by the PubMed algorithm according to calculated 'similarity scores' (see  <a>https://www.ncbi.nlm.nih.gov/books/NBK3827/</a>, 'Computation of Similar Articles'). You can check the positions of your positive controls in the native ranking. Are they present at the topmost positions?</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "__Hits retrieved from PubMed/MEDLINE__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "print(\"\\n\\033[94m\" + \"Similar articles:\" + \"\\033[0m\", sims_count, \"\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "__Check the ranking results of your positives (numerically)__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "pos_positions, pos_positions_df = lola.positives_where(sims_scores_dict, positives)\n",
    "pos_positions_df"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "__Check the ranking results of your positives (visually)__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "print(\"\\033[34m\" + \"\\nFigure 1: Native PubMed Ranking\\n\" + \"\\033[0m\")\n",
    "fig1 = lola.sims_scores2fig(pmid, sims_count, sims_scores_dict, pos_positions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<div class='alert alert-block alert-info'>\n",
    "<p style=\"text-align:justify; font-size:15px;\">For subsequent analyses, the tool has to collect more relevant metadata. This may take about 75 sec/100 hits. PLEASE BE PATIENT!</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "sims_pre = lola.meta4pmids_pandas(lola.sims_dict2pmids(sims_scores_dict))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "__Internal structuring of metadata__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "sims_meta = lola.bodo_func(sims_pre, sims_scores_dict, pmid)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "### __3. GSPsearch_Compare__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<div class='alert alert-block alert-info'>\n",
    "<p style=\"text-align:justify; font-size:15px;\">The tool will now provide details of TWO ALTERNATIVE RANKINGS, A and B, with potentially better ranking performance. Please, compare these (numerical and visual) details, and check the new positions of your positive controls. Are they ranked at top positions? Is the positive control known to be most relevant to you at the topmost position?</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "lola.calibr8(sims_meta, sims_count, ref_length); sims_meta = lola.cScores2ranking(sims_meta)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "__Check alternative rankings (numerically)__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "pos_positions, pos_scores = lola.cPositives_where(positives, sims_meta)\n",
    "pos_positions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "__Check alternative rankings (visually)__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<div class='alert alert-block alert-info'>\n",
    "<p style=\"text-align:justify; font-size:15px;\">The figures below depict the courses of the alternative rankings. Native PubMed rankings are presented as dashed lines for comparison.</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "print(\"\\033[34m\" + \"\\nFigure 2: Alternative Ranking A\\n\" + \"\\033[0m\")\n",
    "fig2 = lola.cScores2fig(pmid, sims_count, sims_meta, pos_positions, pos_scores, 'A')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "print(\"\\033[34m\" + \"\\nFigure 3: Alternative Ranking B\\n\" + \"\\033[0m\")\n",
    "fig3 = lola.cScores2fig(pmid, sims_count, sims_meta, pos_positions, pos_scores, 'B')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "### __4. GSPsearch_Interactive-Reranking__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<div class='alert alert-block alert-info'>\n",
    "<p style=\"text-align:justify; font-size:15px;\">You can now choose, which alternative ranking is conducted to produce the final hit list. As result, you will find a reranked list of the 20 most relevant citations below. This list of PMIDs then can be copied and pasted in the input window of PubMed to retrieve complete citation metadata.</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "choice, i_rank = lola.choose_rerank(positives, sims_meta, 20)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "### __5. GSPsearch_PubMed-Output__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class='alert alert-block alert-info'>\n",
    "<p style=\"text-align:justify; font-size:15px;\">This notebook was not designed as a standalone tool to retrieve all metadata and fulltext links of resulting citations. Please use the PubMed service for these tasks. Simply transfer the string of PMIDs provided below to the PubMed input window with 'copy and paste'.</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "lola.out4pubmed(choice, i_rank)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "### __6. GSPsearch_Alternatives__"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<div class='alert alert-block alert-info'>\n",
    "<p style=\"text-align:justify; font-size:15px;\">The citations (PMIDs) below describe relevant research which doesn't use vertebrate animals.</p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "lola.no_vivo_pmids(sims_meta, i_rank)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
